<?php require('header.php') ?>

        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="static-slider10" style="background-image:url(assets/images/landingpage/banner.jpg); min-height: calc(100vh);">
                    <div class="container">
                        <!-- Row  -->
                        <div class="blog-home1 spacer bg-light" id="explore-demos">
                    <div class="container">
                        <!-- Row  -->
                        <ul>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=62202&view=authors" style="color:white">1. Akuntansi Syariah</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=60202&view=authors" style="color:white">2. Ekonomi Syariah</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=74234&view=authors" style="color:white">3. Hukum Ekonomi Syariah (Muamalah) S1</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=74230&view=authors" style="color:white">4. Hukum Keluarga (Akhwal Syaksiyah)</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=74231&view=authors" style="color:white">5. Hukum Pidana Islam (Jinayah)</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=74235&view=authors" style="color:white">6. Hukum Tatanegara (Siyasah)</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=74201&view=authors " style="color:white">7. Ilmu Hukum</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=61406&view=authors" style="color:white">8. Keuangan dan Perbankan Syariah</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=74233&view=authors" style="color:white">9. Perbandingan Mazhab</a></li>
                        </ul>
                        <!-- Row  -->
                    </div>
                </div>
                    </div>
                </div>
                
            <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
        </div>
    </div>
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="assets/node_modules/popper/dist/popper.min.js"></script>
    <script src="assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
    <!-- This is for the animation -->
    <script src="assets/node_modules/aos/dist/aos.js"></script>
    <script src="assets/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/type.js"></script>
    <script type="text/javascript">
    // This is for counter
    $('.counter').counterUp({
        delay: 10
    });
    /*******************************/
    // this is for the testimonial 3
    /*******************************/
    $('.testi3').owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        dots: true,
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            1170: {
                items: 3
            }
        }
    })
    /*******************************/
    // this is for the slider section
    /*******************************/
    $('.image-slide').owlCarousel({

        autoplay: true,
        autoplayTimeout: 2999,
        autoplaySpeed: 1990,
        autoplayHoverPause: false,
        loop: true,
        margin: 0,
        nav: false,
        dots: false,
        items: 5,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            },
            1920: {
                items: 4
            },
            2170: {
                items: 5
            }
        }
    })
    /*******************************/
    // this is for smooth scroll on click
    /*******************************/
    $('a').on('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 90
        }, 1000);
        event.preventDefault();
        // code

    });
    </script>
</body>

</html>