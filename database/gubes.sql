-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2019 at 02:32 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gubes`
--

-- --------------------------------------------------------

--
-- Table structure for table `d_gubes`
--

CREATE TABLE `d_gubes` (
  `id` int(20) NOT NULL,
  `nama_gubes` varchar(20) NOT NULL,
  `institusi` varchar(50) NOT NULL,
  `b_ilmu` varchar(50) NOT NULL,
  `tahun` varchar(50) NOT NULL,
  `naskah` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d_gubes`
--

INSERT INTO `d_gubes` (`id`, `nama_gubes`, `institusi`, `b_ilmu`, `tahun`, `naskah`) VALUES
(1, 'uy', 'baa', 'eta', '2008', '07022019183035WhatsApp Image 2019-01-16 at 11.31.4'),
(2, 'uy', 'baa', 'eta', '2008', '07022019183111Kendali Publikasi UIN SGD.docx');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `d_gubes`
--
ALTER TABLE `d_gubes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `d_gubes`
--
ALTER TABLE `d_gubes`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
