<?php require('header.php') ?>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Static Slider 10  -->
                <!-- ============================================================== -->
                <div class="static-slider10" style="background-image:url(assets/images/landingpage/ban1.jpg); min-height: calc(100vh);">
                    <div class="container">
                        <!-- Row  -->
                        <div class="row justify-content-center">
                            <!-- Column -->
                            <div class="col-lg-7 col-md-6 align-self-center text-center" data-aos="fade-up" data-aos-duration="1200">
                                <!-- <h1 class="title typewrite" data-type='["Pusat Penelitian", "dan Penerbitan"]'>&nbsp;</h1> -->
                                <h1 class="subtitle font-bold"><font color="#333333">JURNAL TERAKREDITASI</h1></font>
                                <h4 class="subtitle font-light"></h4>
                            </div>
                            <!-- Column -->
                        </div>
                    </div>
                </div>
               <div class="blog-home1 spacer bg-light" id="explore-demos">
                    <div class="container">
                        <!-- Row  -->
                        <div class="row text-center">
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-left" data-aos-duration="1200"><a href="http://sinta2.ristekdikti.go.id/journals/detail?id=3857" target="_blank">
                                    <img class="card-img-top" src="assets/images/sertifikat/S2-1.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Akreditasi Jurnal Ilmu Dakwah (Sinta 2)</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                                <a href="http://sinta2.ristekdikti.go.id/journals/detail?id=25" target="_blank">
                                   <img class="card-img-top" src="assets/images/sertifikat/S2 - 2.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Akreditasi Jurnal Wawasan (Sinta 2)</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                                <a href="http://sinta2.ristekdikti.go.id/journals/detail?id=2574" target="_blank">
                                   <img class="card-img-top" src="assets/images/sertifikat/S2 - 3.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Akreditasi Jurnal Biodjati <br>(Sinta 2)</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-right" data-aos-duration="1200">
                                <a href="http://sinta2.ristekdikti.go.id/journals/detail?id=77" target="_blank">
                                   <img class="card-img-top" src="assets/images/sertifikat/S2 - 4.jpeg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Akreditasi Jurnal Agroteknologi (Sinta 2)</h6>
                                </div>
                            </div>
                                <div class="row text-center">
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-left" data-aos-duration="1200">
                                <a href="http://sinta2.ristekdikti.go.id/journals/detail?id=760" target="_blank">
                                    <img class="card-img-top" src="assets/images/sertifikat/S2 -5.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Akreditasi Jurnal Sertifikat JPI (Sinta 2)</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="100">
                                <a href="http://sinta2.ristekdikti.go.id/journals/detail?id=1306" target="_blank">
                                   <img class="card-img-top" src="assets/images/sertifikat/S3 - 6.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Akreditasi Jurnal Akreditasi JOIN (Sinta 3)</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                                <a href="http://sinta2.ristekdikti.go.id/journals/detail?page=1&id=1919" target="_blank">
                                   <img class="card-img-top" src="assets/images/sertifikat/S3 - 7.jpeg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Akreditasi Jurnal Akreditasi Psympathic (Sinta 3)</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-right" data-aos-duration="1200">
                                <a href="http://sinta2.ristekdikti.go.id/journals/detail?id=3916" target="_blank">
                                   <img class="card-img-top" src="assets/images/sertifikat/S4 - 8.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Akreditasi Jurnal Akreditasi DIROYAH (Sinta 5)</h6>
                                </div>
                            </div>
                            
 <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-left" data-aos-duration="1200">
                                <a href="http://sinta2.ristekdikti.go.id/journals/detail/?id=3909" target="_blank">
                                    <img class="card-img-top" src="assets/images/sertifikat/S4 - 9.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium"> Akreditasi Jurnal Akreditasi RELIGIOUS (Sinta 4)</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="100">
                                <a href="http://sinta2.ristekdikti.go.id/journals/detail/?id=3832" target="_blank">
                                   <img class="card-img-top" src="assets/images/sertifikat/S4 - 10.jpeg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Akreditasi Jurnal Akreditasi Jurnal Analisa (Sinta 4)</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                                 <a href="http://sinta2.ristekdikti.go.id/journals/detail/?id=3836" target="_blank">
                                   <img class="card-img-top" src="assets/images/sertifikat/S4 - 11.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Akreditasi Jurnal Akreditasi TELKA-2018 (Sinta 4)</h6>
                                </div>
                            </div>

                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                                 <a href="http://sinta2.ristekdikti.go.id/journals/detail?id=3830" target="_blank">
                                   <img class="card-img-top" src="assets/images/sertifikat/S4 - 12.jpeg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Akreditasi Jurnal Akreditasi ALBAYAN (Sinta 4)</h6>
                                </div>
                            </div>
                          
                            </div>
                            

                        </div>
                        </div>
                        <!-- Row  -->
                    </div>
                </div>

                 <div class="blog-home1 spacer bg-light" id="explore-demos">
                    <div class="container">
                        <!-- Row  -->
                       
                        <!-- Row  -->
                    </div>
                </div>
            <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
    <?php require('footer.php') ?>

    <script type="text/javascript">
    // This is for counter
    $('.counter').counterUp({
        delay: 10
    });
    /*******************************/
    // this is for the testimonial 3
    /*******************************/
    $('.testi3').owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        dots: true,
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            1170: {
                items: 3
            }
        }
    })
    /*******************************/
    // this is for the slider section
    /*******************************/
    $('.image-slide').owlCarousel({

        autoplay: true,
        autoplayTimeout: 2999,
        autoplaySpeed: 1990,
        autoplayHoverPause: false,
        loop: true,
        margin: 0,
        nav: false,
        dots: false,
        items: 5,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            },
            1920: {
                items: 4
            },
            2170: {
                items: 5
            }
        }
    })
    /*******************************/
    // this is for smooth scroll on click
    /*******************************/
    $('a').on('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 90
        }, 1000);
        event.preventDefault();
        // code

    });
    </script>
</body>

</html>
