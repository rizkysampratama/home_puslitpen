<!DOCTYPE html>
<html lang="en">
<?php 
    require __DIR__ . '/konektor.php';
    setCounterVisitor();
?> 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="puslitpen">
    <meta name="author" content="puslitpen">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/logo/uin.png">
    <title>Puslitpen UIN Sunan Gunung Djati Bandung</title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- <link href="https://cdn.datatables.net/1.10.19/css/dataTables.material.min.css" rel="stylesheet"> -->
    <!-- This is for the animation CSS -->
    <link href="assets/node_modules/aos/dist/aos.css" rel="stylesheet">
    <link href="assets/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="assets/node_modules/owl.carousel/dist/assets/owl.theme.green.css" rel="stylesheet">
    <!-- This page CSS -->
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/index-landingpage/landing-page.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    <script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-85622565-1', 'auto');
    ga('send', 'pageview');
    </script>

    
</head>

<body class="">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Puslitpen</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Top header  -->
        <!-- ============================================================== -->
        <div class="topbar" id="top">
            <div class="header6" >
                <div class="container-fluid po-relative" style="margin-left: 60px;" >
                    <nav class="navbar navbar-expand-lg h6-nav-bar"   >
                        <a href="index.php" class="navbar-brand"><img src="assets/images/logo/logou.png" alt="wrapkit" /></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#h6-info" aria-controls="h6-info" aria-expanded="false" aria-label="Toggle navigation"><span class="ti-menu"></span></button>
                        <div class="collapse navbar-collapse hover-dropdown font-14 ml-auto" id="h6-info">
                            <ul class="navbar-nav ml-10">
                                <li class="nav-item"><a href="index.php" target="_blank" class="nav-link" style="color:gray">Beranda</a></li>
                                <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:gray"> SINTA <i class="fa fa-angle-down m-l-5"></i></a>
                                    <ul class="b-none dropdown-menu font-14 animated fadeInUp">
                                        <li> <a class="dropdown-toggle dropdown-item" href="http://sinta2.ristekdikti.go.id/home/topaffiliations" aria-haspopup="true" aria-expanded="false" target="_blank">Top 100 SINTA Affiliasi</a>
                                        </li>
                                        <li> <a class="dropdown-toggle dropdown-item" href="http://sinta2.ristekdikti.go.id/affiliations/index?q=Universitas+Islam+Negeri&search=1" aria-haspopup="true" aria-expanded="false" target="_blank">Top Affiliasi SINTA Universitas Islam Negeri</a>
                                        </li>
                                        <li> <a class="dropdown-toggle dropdown-item" href="http://sinta2.ristekdikti.go.id/affiliations/detail?id=3511&view=overview
" aria-haspopup="true" aria-expanded="false" target="_blank">SINTA UIN Bandung</a>
                                        </li>
                                        <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" href="javascript:void(0)" aria-haspopup="true" aria-expanded="false">SINTA Fakultas dan Program Studi<i class="fa fa-angle-right ml-auto"></i></a>
                                            <ul class="dropdown-menu menu-right font-14 b-none animated flipInY">
                                                <!-- <li><a class="dropdown-item font-medium">With Sidebar</a></li> -->
                                                <li><a class="dropdown-item" href="ushuludin.php" target="_blank">Ushuludin</a></li>
                                                <li><a class="dropdown-item" href="tarbiyah.php" target="_blank">Tarbiyah dan Keguruan</a></li>
                                                <li><a class="dropdown-item" href="adab.php" target="_blank">Adab dan Humaniora</a></li>
                                                <li><a class="dropdown-item" href="dakwah.php" target="_blank">Dakwah dan Komunikasi</a></li>
                                                <li><a class="dropdown-item" href="saintek.php" target="_blank">Sains dan Teknologi</a></li>
                                                <li><a class="dropdown-item" href="psikologi.php" target="_blank">Psikologi</a></li>
                                                <li><a class="dropdown-item" href="fisip.php" target="_blank">Ilmu Sosial dan Ilmu Politik</a></li>
                                                <li><a class="dropdown-item" href="pascasarjana.php" target="_blank">Pascasarjana</a></li>
                                            </ul>
                                        </li>
                                        <li> <a class="dropdown-toggle dropdown-item" href="http://sinta2.ristekdikti.go.id/affiliations/detail?id=3511&view=authors" aria-haspopup="true" aria-expanded="false">SINTA Author UIN Bandung</a>
                                        </li>

                                    </ul>
                                </li>
                                <li class="nav-item"><a href="isbn.php" target="_blank" class="nav-link" style="color:gray">ISBN</a></li>
                                <li class="nav-item"><a href="jurnal.php" target="_blank" class="nav-link" style="color:gray">Jurnal Terakreditasi</a></li>
                                <li class="nav-item"><a href="https://puslitpen.uinsgd.ac.id/hki" rel="nofollow" target="_blankpage" class="nav-link" style="color:gray">Info HKI</a></li>
                                <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:gray"> Publikasi <i class="fa fa-angle-down m-l-5"></i></a>
                                    <ul class="b-none dropdown-menu font-14 animated fadeInUp">
                                        <li> <a class="dropdown-toggle dropdown-item" href="scopus.php" aria-haspopup="true" aria-expanded="false" target="_blank">Scopus</a>
                                        </li>
                                        <li> <a class="dropdown-toggle dropdown-item" href="webofscience.php" aria-haspopup="true" aria-expanded="false" target="_blank">Web of Science</a>
                                        </li>
                                        <li> <a class="dropdown-toggle dropdown-item" href="publikasinasional.php" aria-haspopup="true" aria-expanded="false" target="_blank">Publikasi Nasional</a>
                                        </li>
                                        <li> <a class="dropdown-toggle dropdown-item" href="internationalbook.php" aria-haspopup="true" aria-expanded="false" target="_blank">International Book</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item"><a href="prestasi.php" target="_blank" class="nav-link" style="color:gray">Prestasi</a></li>
                                <li class="nav-item"><a href="download.php" target="_blank" class="nav-link" style="color:gray">Download</a></li>
                                <li class="nav-item"><a href="berita.php" target="_blank" class="nav-link" style="color:gray">Berita</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>