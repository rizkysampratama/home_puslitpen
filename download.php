<?php require('header.php') ?>
<?php $query = mysqli_query($connect,"SELECT * FROM downloads"); ?>

<style type="text/css">
    #example{
            color: #000;
            font-weight: 500;
    }
    .dataTables_wrapper{
        width: 100%;
    }
</style>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Static Slider 10  -->
                <!-- ============================================================== -->
                <div class="static-slider10" style="background-image:url(assets/images/landingpage/ban1.jpg); min-height: calc(100vh);">
                    <div class="container">
                        <!-- Row  -->
                        <div class="row justify-content-center">
                            <!-- Column -->
                            <div class="col-lg-7 col-md-6 align-self-center text-center" data-aos="fade-up" data-aos-duration="1200">
                                <!-- <h1 class="title typewrite" data-type='["Pusat Penelitian", "dan Penerbitan"]'>&nbsp;</h1> -->
                                <h1 class="subtitle font-bold"><font color="#333333">DOWNLOAD</h1>
                                <h4 class="subtitle font-light"><font color="#333333">Download Dokumen Pusat Penelitian dan Penerbitan</h4>
                            </div>
                            <!-- Column -->
                        </div>
                    </div>
                </div>
                <div class="spacer feature24">
                <div class="container">
                    <!-- Row -->
                    <div class="row justify-content-center">
                        <div class="col-md-7 text-center">
                            <h2 class="title">Download Dokumen Pusat Penelitian dan Penerbitan</h2>
                            <!-- <h6 class="subtitle">Nama-nama Penerbit Online UIN Bandung</h6> -->
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="row wrap-feature-24">
                       <table id="example" class="mdl-data-table" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Dokumen</th>
                                    <th>Link Unduh</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(mysqli_num_rows($query)>0){ ?>
                                <?php
                                    while($data = mysqli_fetch_array($query)){
                                ?>
                                <tr>
                                    <td style="text-align: center;"><?php echo $data["id"];?></td>
                                    <td><?php echo $data["nama_dokumen"];?></td>
                                    <td style="text-align: center;"><a href="<?php echo $data["link"];?>">Unduh</a></td>
                                </tr>
                                <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
        </div>
    <?php require('footer.php') ?>
    <script type="text/javascript">
    // This is for counter
    $('.counter').counterUp({
        delay: 10
    });
    /*******************************/
    // this is for smooth scroll on click
    /*******************************/
    $('a').on('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 90
        }, 1000);
        event.preventDefault();
        // code

    });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
        $('#example').DataTable( {
            columnDefs: [
                {
                    targets: [ 0, 1 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        } );
    } );
    </script>

        <style>
		    table {
		  font-family: Arial, Helvetica, sans-serif;
		  color: #666;
		  text-shadow: 1px 1px 0px #fff;
		  background: #eaebec;
		  border: #ccc 1px solid;
		}
		 
		table th {
		  padding: 15px 35px;
		  border-left:1px solid #e0e0e0;
		  border-bottom: 1px solid #e0e0e0;
		  background: #ededed;
		}
		 
		table th:first-child{  
		  border-left:none;  
		}
		table tr {
		  text-align: left;
		}
		 
		table td:first-child {
		  text-align: left;
		  padding-left: 20px;
		  border-left: 0;
		}
		 
		table td {
		  padding: 15px 35px;
		  border-top: 1px solid #ffffff;
		  border-bottom: 1px solid #e0e0e0;
		  border-left: 1px solid #e0e0e0;
		  background: #fafafa;
		  background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
		  background: -moz-linear-gradient(top, #fbfbfb, #fafafa);
		}
		 
		table tr:last-child td {
		  border-bottom: 0;
		}
		 
		table tr:last-child td:first-child {
		  -moz-border-radius-bottomleft: 3px;
		  -webkit-border-bottom-left-radius: 3px;
		  border-bottom-left-radius: 3px;
		}
		 
		table tr:last-child td:last-child {
		  -moz-border-radius-bottomright: 3px;
		  -webkit-border-bottom-right-radius: 3px;
		  border-bottom-right-radius: 3px;
		}
		 
		table tr:hover td {
		  background: #f2f2f2;
		  background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
		  background: -moz-linear-gradient(top, #f2f2f2, #f0f0f0);
		}
    </style>
</body>

</html>