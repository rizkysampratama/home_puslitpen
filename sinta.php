<?php require('header.php') ?>
    <style type="text/css">
        .wrap-feature-24 {
          margin-top: 60px;
        }

        .wrap-feature-24 .card {
          overflow: hidden;
          -o-transition: 0.3s ease-out;
          transition: 0.3s ease-out;
          -webkit-transition: 0.3s ease-out;
        }

        .wrap-feature-24 .card:hover {
          -ms-transform: translateY(-10px);
          transform: translateY(-10px);
          -webkit-transform: translateY(-10px);
        }

        .wrap-feature-24 .service-24 {
          text-align: center;
          padding: 40px 0;
          display: block;
        }

        .wrap-feature-24 .service-24 i {
          background: #07d79c;
          background: -webkit-linear-gradient(legacy-direction(to right), #07d79c 0%, #1dc8cd 100%);
          background: -webkit-gradient(linear, left top, right top, from(#07d79c), to(#1dc8cd));
          background: -webkit-linear-gradient(left, #07d79c 0%, #1dc8cd 100%);
          background: -o-linear-gradient(left, #07d79c 0%, #1dc8cd 100%);
          background: linear-gradient(to right, #07d79c 0%, #1dc8cd 100%);
          -webkit-background-clip: text;
          background-clip: text;
          -webkit-text-fill-color: transparent;
          text-fill-color: transparent;
          font-size: 50px;
        }

        .wrap-feature-24 .service-24 .ser-title {
          margin: 10px 0 5px;
          font-weight: 500;
        }

        .wrap-feature-24 .service-24:hover, .wrap-feature-24 .service-24:focus {
          background: #07d79c;
          background: -webkit-linear-gradient(legacy-direction(to right), #07d79c 0%, #1dc8cd 100%);
          background: -webkit-gradient(linear, left top, right top, from(#07d79c), to(#1dc8cd));
          background: -webkit-linear-gradient(left, #07d79c 0%, #1dc8cd 100%);
          background: -o-linear-gradient(left, #07d79c 0%, #1dc8cd 100%);
          background: linear-gradient(to right, #07d79c 0%, #1dc8cd 100%);
        }

        .wrap-feature-24 .service-24:hover i, .wrap-feature-24 .service-24:hover .ser-title, .wrap-feature-24 .service-24:focus i, .wrap-feature-24 .service-24:focus .ser-title {
          color: #ffffff;
          text-fill-color: #ffffff;
          -webkit-text-fill-color: #ffffff;
        }

        .wrap-feature-24 .service-24:hover i, .wrap-feature-24 .service-24:focus i {
          margin-bottom: 5px;
        }
    </style>
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Static Slider 10  -->
                <!-- ============================================================== -->
                <div class="static-slider10" style="background-image:url(assets/images/landingpage/banner.jpg); min-height: calc(100vh);">
                    <div class="container">
                        <!-- Row  -->
                        <div class="row justify-content-center">
                            <!-- Column -->
                            <div class="col-lg-7 col-md-6 align-self-center text-center" data-aos="fade-up" data-aos-duration="1200">
                                <!-- <h1 class="title typewrite" data-type='["Pusat Penelitian", "dan Penerbitan"]'>&nbsp;</h1> -->
                                <h1 class="subtitle font-bold">SINTA</h1>
                                <h4 class="subtitle font-light">Science and Technology Index</h4>
                            </div>
                            <!-- Column -->
                        </div>
                    </div>
                </div>
                <div class="spacer feature24">
                <div class="container">
                    <!-- Row -->
                    <div class="row justify-content-center">
                        <div class="col-md-7 text-center">
                            <h2 class="title">Science and Technology Index</h2>
                            <h6 class="subtitle">Jurnal Terindeks SINTA - Universitas Islam Negeri Sunan Gunung Djati Bandung</h6>
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="row wrap-feature-24">
                        <!-- Column -->
                        <div class="col-lg-3 col-md-6">
                            <div class="card card-shadow">
                                <a href="http://sinta2.ristekdikti.go.id/home/topaffiliations" class="service-24" target="_blankpage"> <i class="icon-Target"></i>
                                    <h6 class="ser-title">Top 100 SINTA Affiliasi</h6>
                                </a>
                            </div>
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-lg-3 col-md-6">
                            <div class="card card-shadow">
                                <a href="sinta2.ristekdikti.go.id/affiliations/index?q=Universitas+Islam+Negeri&search=1" class="service-24"> <i class="icon-Car-Wheel" target="_blankpage"></i>
                                    <h6 class="ser-title">Top Affiliasi SINTA Universitas Islam Negeri
</h6>
                                </a>
                            </div>
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-lg-3 col-md-6">
                            <div class="card card-shadow">
                                <a href="javascript:void(0)" class="service-24"> <i class="icon-Mouse-3"></i>
                                    <h6 class="ser-title">SEO Techniques</h6>
                                </a>
                            </div>
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-lg-3 col-md-6">
                            <div class="card card-shadow">
                                <a href="javascript:void(0)" class="service-24"> <i class="icon-Eyeglasses-Smiley"></i>
                                    <h6 class="ser-title">Client Management</h6>
                                </a>
                            </div>
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-lg-3 col-md-6">
                            <div class="card card-shadow">
                                <a href="javascript:void(0)" class="service-24"> <i class="icon-Target-Market"></i>
                                    <h6 class="ser-title">Email Campaign</h6>
                                </a>
                            </div>
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-lg-3 col-md-6">
                            <div class="card card-shadow">
                                <a href="javascript:void(0)" class="service-24"> <i class="icon-Laptop-Phone"></i>
                                    <h6 class="ser-title">Website Strategy</h6>
                                </a>
                            </div>
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-lg-3 col-md-6">
                            <div class="card card-shadow">
                                <a href="javascript:void(0)" class="service-24"> <i class="icon-Full-Bag"></i>
                                    <h6 class="ser-title">eCommerce Shop</h6>
                                </a>
                            </div>
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-lg-3 col-md-6">
                            <div class="card card-shadow">
                                <a href="javascript:void(0)" class="service-24"> <i class="icon-Eyeglasses-Smiley"></i>
                                    <h6 class="ser-title">Cloud Hosting</h6>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
    <?php require('footer.php') ?>

    <script type="text/javascript">
    // This is for counter
    $('.counter').counterUp({
        delay: 10
    });
    /*******************************/
    // this is for the testimonial 3
    /*******************************/
    $('.testi3').owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        dots: true,
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            1170: {
                items: 3
            }
        }
    })
    /*******************************/
    // this is for the slider section
    /*******************************/
    $('.image-slide').owlCarousel({

        autoplay: true,
        autoplayTimeout: 2999,
        autoplaySpeed: 1990,
        autoplayHoverPause: false,
        loop: true,
        margin: 0,
        nav: false,
        dots: false,
        items: 5,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            },
            1920: {
                items: 4
            },
            2170: {
                items: 5
            }
        }
    })
    /*******************************/
    // this is for smooth scroll on click
    /*******************************/
    $('a').on('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 90
        }, 1000);
        event.preventDefault();
        // code

    });
    </script>
</body>

</html>
