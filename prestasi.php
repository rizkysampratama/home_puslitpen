<?php require('header.php') ?>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Static Slider 10  -->
                <!-- ============================================================== -->
                <div class="static-slider10" style="background-image:url(assets/images/landingpage/ban1.jpg); min-height: calc(100vh);">
                    <div class="container">
                        <!-- Row  -->
                        <div class="row justify-content-center">
                            <!-- Column -->
                            <div class="col-lg-7 col-md-6 align-self-center text-center" data-aos="fade-up" data-aos-duration="1200">
                                <!-- <h1 class="title typewrite" data-type='["Pusat Penelitian", "dan Penerbitan"]'>&nbsp;</h1> -->
                                <h1 class="subtitle font-bold"><font color="#333333">PRESTASI</h1></font>
                                <h4 class="subtitle font-light"><font color="#333333">Pusat Penelitian dan Penerbitan UIN Sunan Gunung Djati Bandung</h4></font>
                            </div>
                            <!-- Column -->
                        </div>
                    </div>
                </div>
               <div class="blog-home1 spacer bg-light" id="explore-demos">
                    <div class="container">
                        <!-- Row  -->
                        <div class="row text-center">
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-left" data-aos-duration="1200"><!-- <a href="#" target="_blank"> -->
                                    <img class="card-img-top" src="assets/prestasi/sinta-awards-prof_ali.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">SINTA Award Author Perguruan Tinggi Keagamaan</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                                <!-- <a href="#" target="_blank"> -->
                                   <img class="card-img-top" src="assets/prestasi/sinta-awards-uin-sgd.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">SINTA Awards UIN Sunan Gunung Djati Bandung</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                                <!-- <a href="#" target="_blank"> -->
                                   <img class="card-img-top" src="assets/prestasi/best-stand-in-AICIS.jpeg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">1st Best Stand in AICIS Palu 2018</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-right" data-aos-duration="1200">
                                <!-- <a href="#" target="_blank"> -->
                                   <img class="card-img-top" src="assets/prestasi/prof-arskal.jpeg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">UIN Bandung Inisiator HKI di lingkungan PTKI</h6>
                                </div>
                            </div>
                                
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-right" data-aos-duration="1200">
                                 <a href="https://pdki-indonesia.dgip.go.id/index.php/paten/ZjF1SS8vcHRaaUY0QWRWUXBrL0FYZz09?q=P00201608785&type=1" target="_blank">
                                   <img class="card-img-top" src="assets/prestasi/datapaten.jpeg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <a href="https://pdki-indonesia.dgip.go.id/index.php/paten/ZjF1SS8vcHRaaUY0QWRWUXBrL0FYZz09?q=P00201608785&type=1" target="_blank"></a><h6 class="font-medium">PROSES PEMBUATAN MATERIAL GRAFIT BERBASIS SERAT KAPAS Gossypium sp</h6>
                                </div>
                            </div>

	                         <div class="col-md-3 m-t-20 m-b-30">
	                         	<div class="card card-shadow" data-aos="flip-right" data-aos-duration="1200">
	                            <!-- <a href="#" target="_blank"> -->
	                         	<img class="card-img-top" src="assets/prestasi/1.jpeg" alt=""></a>
	                            </div>
	                            <div class="p-t-10">
	                                <h6 class="font-medium">Pop-up Hook ( Buku Timbul)</h6>
	                            </div>
	                        </div>

	                        <div class="col-md-3 m-t-20 m-b-30">
	                         	<div class="card card-shadow" data-aos="flip-right" data-aos-duration="1200">
	                            <!-- <a href="#" target="_blank"> -->
	                         	<img class="card-img-top" src="assets/prestasi/2.jpeg" alt=""></a>
	                            </div>
	                            <div class="p-t-10">
	                                <h6 class="font-medium">Pembuatan Kefir Kolostrum</h6>
	                            </div>
	                        </div>

	                        <div class="col-md-3 m-t-20 m-b-30">
	                         	<div class="card card-shadow" data-aos="flip-right" data-aos-duration="1200">
	                            <!-- <a href="#" target="_blank"> -->
	                         	<img class="card-img-top" src="assets/prestasi/3.jpeg" alt=""></a>
	                            </div>
	                            <div class="p-t-10">
	                                <h6 class="font-medium">Detoksifikasi melalui Fermentasi oleh Aspergillus Niger</h6>
	                            </div>
	                        </div>
                            </div>
                        </div>
                        </div>
                        <!-- Row  -->
                    </div>
                </div>

                 <div class="blog-home1 spacer bg-light" id="explore-demos">
                    <div class="container">
                        <!-- Row  -->
                       
                        <!-- Row  -->
                    </div>
                </div>
            <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
<?php require('footer.php') ?>

    <script type="text/javascript">
    // This is for counter
    $('.counter').counterUp({
        delay: 10
    });
    /*******************************/
    // this is for the testimonial 3
    /*******************************/
    $('.testi3').owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        dots: true,
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            1170: {
                items: 3
            }
        }
    })
    /*******************************/
    // this is for the slider section
    /*******************************/
    $('.image-slide').owlCarousel({

        autoplay: true,
        autoplayTimeout: 2999,
        autoplaySpeed: 1990,
        autoplayHoverPause: false,
        loop: true,
        margin: 0,
        nav: false,
        dots: false,
        items: 5,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            },
            1920: {
                items: 4
            },
            2170: {
                items: 5
            }
        }
    })
    /*******************************/
    // this is for smooth scroll on click
    /*******************************/
    $('a').on('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 90
        }, 1000);
        event.preventDefault();
        // code

    });
    </script>
</body>

</html>
