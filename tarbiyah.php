<?php require('header.php') ?>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Static Slider 10  -->
                <!-- ============================================================== -->
                <div class="static-slider10" style="background-image:url(assets/images/landingpage/banner.jpg); min-height: calc(100vh);">
                    <div class="container">
                        <!-- Row  -->
                        <div class="row justify-content-center">
                            <!-- Column -->
                            <div class="col-lg-7 col-md-6 align-self-center text-center" data-aos="fade-up" data-aos-duration="1200">
                                <!-- <h1 class="title typewrite" data-type='["Pusat Penelitian", "dan Penerbitan"]'>&nbsp;</h1> -->
                                <h1 class="subtitle font-bold">SINTA Fakultas Tarbiyah dan Keguruan</h1>
                                <h4 class="subtitle font-light">Science and Technology Index</h4>
                            </div>
                            <!-- Column -->
                        </div>
                    </div>
                </div>
                <div class="spacer feature24">
                <div class="container">
                    <!-- Row -->
                    <div class="row justify-content-center">
                        <div class="col-md-7 text-center">
                            <h2 class="title">Fakultas Tarbiyah dan Keguruan</h2>
                            <h6 class="subtitle">Jurnal Terindeks SINTA - Fakultas Tarbiyah dan Keguruan</h6>
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="row wrap-feature-24">
                        <ol>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=86216&view=authors" style="color:black">DSM Pendidikan Agama Islam</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=86235&view=authors" style="color:black">DSM Pendidikan Guru Madrasah Ibtidaiyah</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=86231&view=authors" style="color:black">Manajemen Pendidikan Islam S1</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=86208&view=authors" style="color:black">Pendidikan Islam S1</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=88204&view=authors" style="color:black">Pendidikan Bahasa Arab S1</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=88203&view=authors" style="color:black">Pendidikan Bahasa Inggris</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=84205&view=authors" style="color:black">Pendidikan Biologi</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=84203&view=authors" style="color:black">Pendidikan Fisika</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=86232&view=authors" style="color:black">Pendidikan Guru Madrasyah Ibtidaiyah</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=86207&view=authors" style="color:black">Pendidikan Anak Usia Dini</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=84204&view=authors" style="color:black">Pendidikan Kimia</a></li>
                            <li><a href="http://sinta2.ristekdikti.go.id/departments/detail?afil=3511&id=84202&view=authors" style="color:black">Pendidikan Matematika</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
<?php require('footer.php') ?>

    <script type="text/javascript">
    // This is for counter
    $('.counter').counterUp({
        delay: 10
    });
    /*******************************/
    // this is for the testimonial 3
    /*******************************/
    $('.testi3').owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        dots: true,
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            1170: {
                items: 3
            }
        }
    })
    /*******************************/
    // this is for the slider section
    /*******************************/
    $('.image-slide').owlCarousel({

        autoplay: true,
        autoplayTimeout: 2999,
        autoplaySpeed: 1990,
        autoplayHoverPause: false,
        loop: true,
        margin: 0,
        nav: false,
        dots: false,
        items: 5,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            },
            1920: {
                items: 4
            },
            2170: {
                items: 5
            }
        }
    })
    /*******************************/
    // this is for smooth scroll on click
    /*******************************/
    $('a').on('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 90
        }, 1000);
        event.preventDefault();
        // code

    });
    </script>
</body>

</html>
