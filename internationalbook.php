<?php require('header.php') ?>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Static Slider 10  -->
                <!-- ============================================================== -->
                <div class="static-slider10" style="background-image:url(assets/images/landingpage/ban1.jpg); min-height: calc(100vh);">
                    <div class="container">
                        <!-- Row  -->
                        <div class="row justify-content-center">
                            <!-- Column -->
                            <div class="col-lg-7 col-md-6 align-self-center text-center" data-aos="fade-up" data-aos-duration="1200">
                                <!-- <h1 class="title typewrite" data-type='["Pusat Penelitian", "dan Penerbitan"]'>&nbsp;</h1> -->
                                <h1 class="subtitle font-bold"><font color="#333333">International Book</h1></font>
                                <!-- <h4 class="subtitle font-light">International Standard Book Number</h4> -->
                            </div>
                            <!-- Column -->
                        </div>
                    </div>
                </div>
                <div class="spacer feature24">
                <div class="container">
                    <!-- Row -->
                    <div class="row justify-content-center">
                        <div class="col-md-7 text-center">
                            <h2 class="title">Buku Tingkat Internasional </h2>
                            <h2 class="subtitle">UIN SGD BANDUNG</h2>
                            <br><br>
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="row wrap-feature-24">
                        <ol type="1">
                            <li>
                                <a href="https://www.taylorfrancis.com/books/9780203878545" style="color:black">Nina Nurmila, Women, Islam And Everyday Life: Renegotiating Polygamy In Indonesia (London: Routledge, 2009).
                                </a>
                            </li>
                            <li>
                                <a href="https://my.scholars-press.com/catalogue/details/gb/978-3-639-76150-4/entrepreneurship-of-traditionalist-muslim?search=Entrepreneurship%20of%20Traditionalist%20Muslim%20in%20Tasikmalaya" style="color:black">Yadi Janwari, Entrepreneurship of Traditionalist Muslim in Tasikmalaya, Indonesia (Saarbrücken, Jerman: Scholars-Press, 2015)
                                </a>
                            </li>
                            <li>
                                <a href="https://www.langer-blomqvist.de/blasphemy-and-politics-in-indonesia-zulkarnain-fisher-lap-lambert-academic-publishing-9786139879519.html" style="color:black">Fisher Zulkarnaen, Blasphemy and Politics in Indonesia (Saarbrücken, Germany: LAP Lambert Academic Publishing, 2018)
                                </a>
                            </li>
                            <li>
                                <a href="http://digitool.library.mcgill.ca/R/?func=dbin-jump-full&object_id=20150&local_base=GEN01-MCG02" style="color:black">Erni Haryanti, Haji Agus Salim: His role in nationalist movements in Indonesia during the early twentieth century. (Thesis McGill 1996)
                                </a>
                            </li>
                            <li>
                                <a href="https://www.amazon.com/Equal-Representation-Women-Parliament-Indonesia%C2%BFs/dp/3639125789" style="color:black">Erni Haryanti. Equal Representation of Women in Parliament: Equality, Representation of Women, Parliament, Indonesia’s Democratic Transition (Saarbrücken, Germany: VDM Verlag Dr. Müller, 2009).
                                </a>
                            </li>
                            <li>
                                <a href="https://openresearch-repository.anu.edu.au/handle/1885/147970" style="color:black">M Hasbullah, The making of hegemony : cultural presentations of the Muslim middle class in Indonesian new order period (Australian National University, 1999)
                                </a>
                            </li>
                            <li>
                                <a href="https://www.taylorfrancis.com/books/9781315849805" style="color:black">Nurmila, N, Bennett, L.R, “The Sexual Politics Of Polygamy In Indonesian Marriages”, at Linda Rae Bennett, Sharyn Graham Davies, Sex and Sexualities in Contemporary Indonesia: Sexual Politics, Health, Diversity and Representations (London: Routledge, 2014).
                                </a>
                            </li>
                            <li>
                                <a href="https://link.springer.com/chapter/10.1007%2F978-3-319-30279-9_12" style="color:black">Aceng Sambas, Sundarapandian Vaidyanathan, Mustafa Mamat, W. S. Mada Sanjaya, Darmawan Setia Rahayu
                                </a>
                            </li>
                            <li>
                                <a href="https://link.springer.com/chapter/10.1007%2F978-3-319-30340-6_6" style="color:black">Aceng Sambas, Mada Sanjaya WS, Mustafa Mamat, Rizki Putra Prastio
                                </a>
                            </li>
                            <li>
                                <a href="https://link.springer.com/chapter/10.1007%2F978-3-319-71243-7_16" style="color:black">Aceng Sambas, Sundarapandian Vaidyanathan, Mustafa Mamat, W. S. Mada Sanjaya
                                </a>
                            </li>
                            <li> Book Chapter:
                                <ol type="a">
                                    <li> 
                                        <a href="http://www.publishing.monash.edu/books/hm-9781925495553.html" style="color:black">Jajang A Rohmana, “The Doctrin of Seven Grades in Hasan Mustapa’s Verse,” in Julian Millie ed., Hasan Mustapa: Ethnicity and Islam in Indonesia (Monash Publishing University, 2017)
                                        </a>
                                    </li>
                                    <li> 
                                        <a href="http://www.publishing.monash.edu/books/hm-9781925495553.html" style="color:black">Asep Salahudin, “The Solitary Man: Hasan Mustapa’s Spirituality in The Present” in Julian Millie ed., Hasan Mustapa: Ethnicity and Islam in Indonesia (Monash Publishing University, 2017)
                                        </a>
                                    </li>
                                    <li> 
                                        <a href="http://www.publishing.monash.edu/books/hm-9781925495553.html" style="color:black">Ahmad Gibson Al-Bustomi, “The teaching of our islamic inheritance: The drama of the human lifesource,” in Julian Millie ed., Hasan Mustapa: Ethnicity and Islam in Indonesia (Monash Publishing University, 2017)
                                        </a>
                                    </li>
                                </ol>
                            </li>
                            <li> 
                                <a href="https://books.google.co.id/books?id=o1RwknhNOJsC&pg=PT17&hl=id&source=gbs_toc_r&cad=4#v=onepage&q&f=false" style="color:black">Erni Haryanti, “Hadji Agus Salim Sumatera-Java, 1884-1954,” Charles Kurzman, ed., Modernist Islam 1840-1940: A Sourcebook (Oxford: Oxford University Press, 2002).
                                </a>
                            </li>
                            <li> 
                                <a href="https://link.springer.com/article/10.1007%2Fs11562-018-0427-9" style="color:black">Moch Fakhruroji
                                </a>
                            </li>
                            <li> 
                                <a  style="color:black">Agus Ahmad Safei, Islam And Socio-Ecological Problems: How Religion Works In Regard To Earth Theology, Man in India: 96 (8), pp. 2493-2502, 2016.
                                </a>
                            </li>
                            <li> 
                                <a style="color:black">H. Sulasman, “ The Islamic Law in Indonesia in Historical Perspective”, at Ahmad Al-Dubayan, The Islamic Quarterly (London: The Islamic Cultural Center and London Central Mosque, 2018)
                                </a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
<?php require('footer.php') ?>

    <script type="text/javascript">
    // This is for counter
    $('.counter').counterUp({
        delay: 10
    });
    /*******************************/
    // this is for the testimonial 3
    /*******************************/
    $('.testi3').owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        dots: true,
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            1170: {
                items: 3
            }
        }
    })
    /*******************************/
    // this is for the slider section
    /*******************************/
    $('.image-slide').owlCarousel({

        autoplay: true,
        autoplayTimeout: 2999,
        autoplaySpeed: 1990,
        autoplayHoverPause: false,
        loop: true,
        margin: 0,
        nav: false,
        dots: false,
        items: 5,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            },
            1920: {
                items: 4
            },
            2170: {
                items: 5
            }
        }
    })
    /*******************************/
    // this is for smooth scroll on click
    /*******************************/
    $('a').on('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 90
        }, 1000);
        event.preventDefault();
        // code

    });
    </script>
</body>

</html>
