<?php require('header.php') ?>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Static Slider 10  -->
                <!-- ============================================================== -->
                <div class="static-slider10" style="background-image:url(assets/images/landingpage/ban1.jpg); min-height: calc(100vh);">
                    <div class="container">
                        <!-- Row  -->
                        <div class="row justify-content-center">
                            <!-- Column -->
                            <div class="col-lg-7 col-md-6 align-self-center text-center" data-aos="fade-up" data-aos-duration="1200">
                                <!-- <h1 class="title typewrite" data-type='["Pusat Penelitian", "dan Penerbitan"]'>&nbsp;</h1> -->
                                <h1 class="subtitle font-bold"><font color="#333333">ISBN</h1></font>
                                <h4 class="subtitle font-light"><font color="#333333">International Standard Book Number</h4></font>
                            </div>
                            <!-- Column -->
                        </div>
                    </div>
                </div>
                <div class="spacer feature24">
                <div class="container">
                    <!-- Row -->
                    <div class="row justify-content-center">
                        <div class="col-md-7 text-center">
                            <!-- <h2 class="title">International Standard Book Number</h2> -->
                            <h2 class="subtitle">Penerbit Online ISBN UIN Bandung</h2>
                            <br><br>
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="row wrap-feature-24">
                        <ol>
                            <li>
                                <a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Fakultas+Dakwah+dan+Komunikasi+UIN+Sunan+Gunung+Djati+Bandung" style="color:black">Fakultas Dakwah dan Komunikasi UIN Sunan Gunung Djati Bandung
                                </a>
                            </li>
                            <li>
                                <a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Jurusan+Manajemen+Dakwah+Fakultas+Dakwah+dan+Komunikasi+UIN+Sunan+Gunung+Djati+Bandung" style="color:black">Jurusan Manajemen Dakwah Fakultas Dakwah dan Komunikasi UIN Sunan Gunung Djati Bandung
                                </a>
                            </li>
                            <li>
                                <a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Jurusan+Manajemen+Dakwah+Fakultas+Dakwah+dan+Komunikasi+UIN+Sunan+Gunung+Djati+Bandung" style="color:black">Fakultas Ushuluddin UIN Sunan Gunung Djati Bandung
                                </a>
                            </li>
                            <li>
                                <a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Fakultas+Ushuluddin+UIN+Sunan+Gunung+Djati+Bandung" style="color:black">Religious Studies Development Program (RSDP) Pascasarjana Uin Sunan Gunung Djati
                                </a>
                            </li>
                            <li>
                                <a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Jurusan+Bahasa+dan+Sastra+Arab+Fakultas+Adab+dan+Humaniora+UIN+Sunan+Gunung+Djati" style="color:black">Jurusan Bahasa dan Sastra Arab Fakultas Adab dan Humaniora UIN Sunan Gunung Djati
                                </a>
                            </li>
                            <li><a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Fakultas+Sains+dan+Teknologi+Universitas+Islam+Negeri+Sunan+Gunung+Djati+Bandung" style="color:black">Fakultas Sains dan Teknologi Universitas Islam Negeri Sunan Gunung Djati Bandung

</a></li>
                            <li><a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Jurusan+Biologi+Fakultas+Sains+dan+Teknologi+UIN+SGD+Bandung" style="color:black">Jurusan Biologi Fakultas Sains dan Teknologi UIN SGD Bandung
</a></li>
                            <li><a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Jurusan+Kimia+Fakultas+Sains+dan+Teknologi+UIN+Sunan+Gunung+Djati+Bandung
" style="color:black">Jurusan Kimia Fakultas Sains dan Teknologi UIN Sunan Gunung Djati Bandung</a></li>
                            <li><a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Pusat+Pengabdian+Kepada+Masyarakat+LP2M+UIN+Sunan+Gunung+Djati+Bandung" style="color:black">Pusat Pengabdian Kepada Masyarakat LP2M UIN Sunan Gunung Djati Bandung</a></li>
                            <li>
                                <a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Pusat+Penelitian+dan+Penerbitan+UIN+SGD+Bandung" style="color:black">Pusat Penelitian dan Penerbitan UIN SGD Bandung
                                </a>
                            </li>
                            <li>
                                <a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Program+Pascasarjana+UIN+Sunan+Gunung+Djati+Bandung" style="color:black">Program Pascasarjana UIN Sunan Gunung Djati Bandung (On Process)
                                </a>
                            </li>

                            <li><a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Judul&searchTxt=Konsorsium+Keil
muan+Wahyu+Memandu+Ilmu+UIN+Sunan+Gunung+Djati+Bandung" style="color:black">Konsorsium Keilmuan Wahyu Memandu Ilmu UIN Sunan Gunung Djati Bandung (On Process)</a>
</li>
                            <li><a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Jurusan+Adm
inistrasi+Publik+FISIP+UIN+SGD+Bandung" style="color:black">Jurusan Administrasi Publik FISIP UIN SGD Bandung (on proses)</a>
                            </li>
                            <li><a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Program+Stu
di+Sastra+Inggris+UIN+SGD" style="color:black">Program Studi Sastra Inggris UIN SGD (On Process)</a>
                            </li>
                            <li><a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Jurusan+Teknik+Elektro+Fakultas+Sains+dan+Teknologi+UIN+Sunan+Gunung+Djati+Bandung
" style="color:black">Jurusan Teknik Elektro Fakultas Sains dan Teknologi UIN Sunan Gunung Djati Bandung</a>
                            </li>
                            <li>
                                <a href="https://isbn.perpusnas.go.id/Account/SearchBuku?searchCat=Penerbit&searchTxt=Fakultas+Syariah+dan+Hukum+UIN+Sunan+Gunung+Djati+Bandung" style="color:black">Fakultas Syariah dan Hukum UIN Sunan Gunung Djati Bandung
                                </a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
<?php require('footer.php') ?>

    <script type="text/javascript">
    // This is for counter
    $('.counter').counterUp({
        delay: 10
    });
    /*******************************/
    // this is for the testimonial 3
    /*******************************/
    $('.testi3').owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        dots: true,
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            1170: {
                items: 3
            }
        }
    })
    /*******************************/
    // this is for the slider section
    /*******************************/
    $('.image-slide').owlCarousel({

        autoplay: true,
        autoplayTimeout: 2999,
        autoplaySpeed: 1990,
        autoplayHoverPause: false,
        loop: true,
        margin: 0,
        nav: false,
        dots: false,
        items: 5,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            },
            1920: {
                items: 4
            },
            2170: {
                items: 5
            }
        }
    })
    /*******************************/
    // this is for smooth scroll on click
    /*******************************/
    $('a').on('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 90
        }, 1000);
        event.preventDefault();
        // code

    });
    </script>
</body>

</html>
