<?php require('header.php') ?>

        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="static-slider10" style="background-image:url(assets/images/landingpage/banner.jpg); min-height: calc(100vh);">
                    <div class="container">
                        <!-- Row  -->
                        <div class="blog-home1 spacer bg-light" id="explore-demos">
                    <div class="container">
                        <!-- Row  -->
						<h3><font color=white>Galeri Foto Kegiatan</h3></font>
						<div class="row text-center">
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-left" data-aos-duration="1200">
                                    <a href="../all-demos/demo-creative/index.html" target="_blank"><img class="card-img-top" src="assets/images/icon/research.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Foto 1</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                                    <a href="../all-demos/demo-business/index.html" target="_blank"><img class="card-img-top" src="assets/images/icon/publication.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Foto 2</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                                    <a href="../all-demos/demo-cryptocurrency/index.html" target="_blank"><img class="card-img-top" src="assets/images/icon/agenda.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Foto 3</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-right" data-aos-duration="1200">
                                    <a href="../all-demos/demo-software-application/index.html" target="_blank"><img class="card-img-top" src="assets/images/icon/pengumuman.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Foto 4</h6>
                                </div>
                            </div>
                        </div>
                        <!-- Row  -->
                    </div>
                </div>
                    </div>
                </div>
                
            <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
        </div>
    </div>
<?php require('footer.php') ?>

    <script type="text/javascript">
    // This is for counter
    $('.counter').counterUp({
        delay: 10
    });
    /*******************************/
    // this is for the testimonial 3
    /*******************************/
    $('.testi3').owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        dots: true,
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            1170: {
                items: 3
            }
        }
    })
    /*******************************/
    // this is for the slider section
    /*******************************/
    $('.image-slide').owlCarousel({

        autoplay: true,
        autoplayTimeout: 2999,
        autoplaySpeed: 1990,
        autoplayHoverPause: false,
        loop: true,
        margin: 0,
        nav: false,
        dots: false,
        items: 5,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            },
            1920: {
                items: 4
            },
            2170: {
                items: 5
            }
        }
    })
    /*******************************/
    // this is for smooth scroll on click
    /*******************************/
    $('a').on('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 90
        }, 1000);
        event.preventDefault();
        // code

    });
    </script>
</body>

</html>
