<?php require('header.php') ?>
        <!-- ============================================================== -->
        <!-- Top header  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Static Slider 10  -->
                <!-- ============================================================== -->
                <div class="static-slider10" style="background-image:url(assets/images/landingpage/ban1.jpg); min-height: calc(100vh);">
                    <div class="container">
                        <!-- Row  -->
                        <div class="row justify-content-center">
                            <!-- Column -->
                            <div class="col-lg-7 col-md-6 align-self-center text-center" data-aos="fade-up" data-aos-duration="1200">
                                <!-- <h1 class="title typewrite" data-type='["Pusat Penelitian", "dan Penerbitan"]'>&nbsp;</h1> -->
                                <h1 class="subtitle font-bold"><font color="#333333">Berita</h1></font>
                                <h4 class="subtitle font-light"></h4>
                            </div>
                            <!-- Column -->
                        </div>
                    </div>
                </div>
                <div class="blog-home1 spacer bg-light" id="explore-demos">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-7 text-center">
                                <h2 class="title" style="color: white;">Berita</h2>
                                <!-- <h6 class="subtitle">Nama-nama Penerbit Online UIN Bandung</h6> -->
                            </div>
                        </div>
                        <ol style="color: white;">
                            <li>
                                <a href="uin_sgd_laksanakan_seminar_proposal.php" style="color:white; font-weight: 200px;">06 Mei 2019 - UIN Bandung Laksanakan Seminar Proposal
                                </a>
                            </li>
                            <li>
                                <a href="http://www.galamedianews.com/bandung-raya/191331/uin-bandung-raih-5-penghargaan.html" style="color:white; font-weight: 200px;">26 Juni 2018 - UIN Bandung Raih 5 Penghargaan
                                </a>
                            </li>
                            <li>
                                <a href="https://www.ristekdikti.go.id/penghargaan-sinta-awards-tahun-2018/" style="color:white; font-weight: 200px;">04 Juli 2018 - Penghargaan SINTA Award 2018
                                </a>
                            </li>
                            <li>
                                <a href="http://www.galamedianews.com/bandung-raya/192210/uin-sgd-bandung-raih-sinta-awards-2018-kategori-perguruan-tinggi-keagamaan.html" style="color:white; font-weight: 200px;">05 juli 2018 - UIN Bandung raih SINTA AWARD 2018 Kategori Perguruan Tinggi Keagamaan
                                </a>
                            </li>
                            <li>
                                <a href="http://nusantara.rmol.co/read/2018/07/05/346448/Penelitian-Banyak-Masuk-Jurnal-Internasional,-UIN-Bandung-Sabet-Sinta-Award-2018-" style="color:white; font-weight: 200px;">05 juli 2018 - Banyak Masuk Jurnal Internasional
                                </a>
                            </li>
                            <li>
                                <a href="https://www.referenesia.com/uin-sgd-bandung-serentak-ajukan-akreditasi-jurnal/" style="color:white; font-weight: 200px;">02 September 2018 - UIN SGD Bandung Serentaak Ajukan Akreditasi Jurnal
                                </a>
                            </li>
                            <li>
                                <a href="http://uinsgd.ac.id/kolom/metode-merawat-jurnal-ilmiah/amp/" style="color:white; font-weight: 200px;">03 september 2018 - Metode Merawat Jurnal Ilmiah
                                </a>
                            </li>
                            <li>
                                <a href="https://www.referenesia.com/metode-merawat-jurnal-ilmiah/" style="color:white; font-weight: 200px;">03 september 2018 - Metode Merawat Jurnal Ilmiah
                                </a>
                            </li>
                            <li>
                                <a href="https://www.republika.co.id/berita/dunia-islam/islam-nusantara/18/09/20/pfch1i313-uin-bandung-meriahkan-ekspo-aicis" style="color:white; font-weight: 200px;">20 September 2018 - UIN Bandung Meriahkan Expo AICIS
                                </a>
                            </li>
                            <li>
                                <a href="https://www.republika.co.id/berita/dunia-islam/islam-nusantara/18/09/27/pfpiuk313-uin-bandung-patenkan-hasil-riset" style="color:white; font-weight: 200px;">27 September 2018 - UIN Bandung Patenkan Hasil Riset
                                </a>
                            </li>
                            <li>
                                <a href="https://www.republika.co.id/berita/pendidikan/dunia-kampus/18/10/24/ph3few430-uin-sgd-berkomitmen-tingkatkan-kualitas-penelitian" style="color:white; font-weight: 200px;">24 Oktober 2018 - UIN Bandung Tingkatkan Kualitas Penelitian
                                </a>
                            </li>
                            <li>
                                <a href="https://www.referenesia.com/uin-sgd-bandung-pacu-bidang-penelitian-menuju-perguruan-tinggi-akreditasi-a/" style="color:white; font-weight: 200px;">27 Oktober 2018 - UIN SGD Bandung Pacu Bidang Penelitian Menuju Perguruan Tinggi Akreditasi A
                                </a>
                            </li>
                            <li>
                                <a href="http://www.galamedianews.com/bandung-raya/204668/uin-bandung-evaluasi-kinerja-publikasi-20162018.html" style="color:white; font-weight: 200px;">30 Oktober 2018 - UIN SGD Bandung Evaluasi Kinerja Publikasi
                                </a>
                            </li>
                            <li>
                                <a href="https://www.referenesia.com/pecah-rekor-13-jurnal-uin-bandung-terakreditasi-nasional-2018/" style="color:white; font-weight: 200px;">02 November 2018 - Jurnal UIN Bandung Terakreditasi Nasional
                                </a>
                            </li>
                            <li>
                                <a href="http://www.galamedianews.com/bandung-raya/204989/13-jurnal-uin-bandung-terakreditasi-nasional-2018.html" style="color:white; font-weight: 200px;">02 November 2018 - Jurnal UIN Bandung Terakreditasi Nasional
                                </a>
                            </li>
                            <li>
                                <a href="https://www.referenesia.com/optimis-dapat-akreditasi-a-uin-sgd-bandung-bertengger-menjuarai-posisi-puncak-di-web-sinta-ristekdikti-mengalahkan-ptki-se-indonesia/" style="color:white; font-weight: 200px;">03 Maret 2019 - Optimis Dapat Akreditasi A : UIN SGD Bandung Bertengger Menjuarai Posisi Puncak di Web Sinta Ristekdikti mengalahkan PTKI se Indonesia
                                </a>
                            </li>
                            <li>
                                <a href="https://www.referenesia.com/terus-berlari-uin-sgd-bandung-raih-353-karya-ilmiah-ter-index-scopus/" style="color:white; font-weight: 200px;">03 Maret 2019 - Terus Berlari :UIN SGD Bandung raih 353 Karya Ilmiah ter-Index Scopus
                                </a>
                            </li>
                        </ol>
                        <!-- Row  -->
                        <div class="row text-center">
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-left" data-aos-duration="1200">
                                    <a href="http://www.galamedianews.com/bandung-raya/191331/uin-bandung-raih-5-penghargaan.html" target="_blank"><img class="card-img-top" src="assets/images/berita/UIN_SGD_Raih_5_Penghargaan.jpeg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">UIN SGD Raih 5 Penghargaan</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                                    <a href="https://www.ristekdikti.go.id/penghargaan-sinta-awards-tahun-2018/" target="_blank"><img class="card-img-top" src="assets/images/berita/sinta_award_full.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Penghargaan Sinta Awards 2018</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                                    <a href="https://www.referenesia.com/uin-sgd-bandung-serentak-ajukan-akreditasi-jurnal/" target="_blank"><img class="card-img-top" src="assets/images/berita/uin_sgd_bandung_ajukan_akreditasi_9_jurnal_ilmiah.jpg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">UIN SGD Ajukan Akreditasi Jurnal Ilmiah</h6>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-3 m-t-20 m-b-30">
                                <div class="card card-shadow" data-aos="flip-right" data-aos-duration="1200">
                                    <a href="https://www.referenesia.com/uin-sgd-bandung-pacu-bidang-penelitian-menuju-perguruan-tinggi-akreditasi-a/" target="_blank"><img class="card-img-top" src="assets/images/berita/Perjuangan_SGD_Tak_Pernah_Putus.jpeg" alt="wrappixel kit"></a>
                                </div>
                                <div class="p-t-10">
                                    <h6 class="font-medium">Perjuangan SGD Tak Pernah Pupus</h6>
                                </div>
                            </div>
                        </div>
                        <!-- Row  -->
                    </div>
                </div>
            <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
 <?php require('footer.php') ?>

    <script type="text/javascript">
    // This is for counter
    $('.counter').counterUp({
        delay: 10
    });
    /*******************************/
    // this is for the testimonial 3
    /*******************************/
    $('.testi3').owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        dots: true,
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            1170: {
                items: 3
            }
        }
    })
    /*******************************/
    // this is for the slider section
    /*******************************/
    $('.image-slide').owlCarousel({

        autoplay: true,
        autoplayTimeout: 2999,
        autoplaySpeed: 1990,
        autoplayHoverPause: false,
        loop: true,
        margin: 0,
        nav: false,
        dots: false,
        items: 5,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            },
            1920: {
                items: 4
            },
            2170: {
                items: 5
            }
        }
    })
    /*******************************/
    // this is for smooth scroll on click
    /*******************************/
    $('a').on('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 90
        }, 1000);
        event.preventDefault();
        // code

    });
    </script>
</body>

</html>
