<?php require('header.php') ?>
        <!-- ============================================================== -->
        <!-- Top header  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Static Slider 10  -->
                <!-- ============================================================== -->
                <div class="static-slider10" style="background-image:url(assets/images/landingpage/ban1.jpg); min-height: calc(100vh);">
                    <div class="container">
                        <!-- Row  -->
                        <div class="row justify-content-center">
                            <!-- Column -->
                            <div class="col-lg-7 col-md-6 align-self-center text-center" data-aos="fade-up" data-aos-duration="1200">
                                <!-- <h1 class="title typewrite" data-type='["Pusat Penelitian", "dan Penerbitan"]'>&nbsp;</h1> -->
                                <h1 class="subtitle font-bold"><font color="#333333">Berita</h1></font>
                                <h4 class="subtitle font-light"></h4>
                            </div>
                            <!-- Column -->
                        </div>
                    </div>
                </div>
                <div class="blog-home1 spacer bg-light" id="explore-demos">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-7 text-center">
                                <h2 class="title" style="color: white;">UIN Bandung Laksanakan Seminar Proposal</h2>
                                <!-- <h6 class="subtitle">Nama-nama Penerbit Online UIN Bandung</h6> -->
                            </div>
                        </div>
                        <ol style="color: white;">
                            <a href="uin_sgd_laksanakan_seminar_proposal.php" style="color:white; font-weight: 200px;">
                                &ensp;&ensp;&ensp;Kompetisi bantuan, setelah tahap I dan tahap II, giliran memasuki tahap III, yaitu seminar proposal. Telah diberitakan sebelumnya, tahap I adalah seleksi administrasi, dan tahap II ialah seleksi substansi.
                                <br><br>

                                &ensp;&ensp;&ensp;Berdasarkan tahap II muncul tiga kategori, yakni: <br>1) lolos; <br>2) dipertimbangkan; dan <br>3) gagal. Kategori lolos dan dipertimbangkan akan diumumkan dalam waktu dekat untuk mengikuti tahap III seminar atau presentasi proposal.
                                <br><br>

                                &ensp;&ensp;&ensp;Tahap III seminar proposal bertujuan pendalaman materi, komitmen output dan outcome, dan tinjauan rencana anggaran biaya (RAB). Secara teknis, tahap III akan dituangkan ke dalam instruksi kerja.
                                <br><br>

                                &ensp;&ensp;&ensp;Hasil tahap III menjadi dasar untuk menerbitkan Surat Keputusan Rektor tentang Penerima Bantuan Penelitian, Pengabdian kepada Masyarakat, dan Publikasi Ilmiah. Surat keputusan ini meliputi nama ketua dan anggota, revisi judul --bila ada revisi, dan nominatif.
                                <br><br>

                                &ensp;&ensp;&ensp;Untuk hal-hal yang belum jelas mohon menghubungi Lembaga Penelitian dan Pengabdian kepada Masyarakat (LP2M) Universitas Islam Negeri (UIN) Sunan Gunung Djati (SGD) Bandung.
                                <br><br>

                                Kantor Tata Usaha LP2M UIN SGD buka setiap hari pukul 08.30-15.00 WIB.
                                <br><br>
                                Bandung, 06 Mei 2019
                                <br><br>
                                LP2M UIN SGD Bandung
                            </a>
                        </ol>
                    </div>
                </div>
            <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
 <?php require('footer.php') ?>

    <script type="text/javascript">
    // This is for counter
    $('.counter').counterUp({
        delay: 10
    });
    /*******************************/
    // this is for the testimonial 3
    /*******************************/
    $('.testi3').owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        dots: true,
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            1170: {
                items: 3
            }
        }
    })
    /*******************************/
    // this is for the slider section
    /*******************************/
    $('.image-slide').owlCarousel({

        autoplay: true,
        autoplayTimeout: 2999,
        autoplaySpeed: 1990,
        autoplayHoverPause: false,
        loop: true,
        margin: 0,
        nav: false,
        dots: false,
        items: 5,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            },
            1920: {
                items: 4
            },
            2170: {
                items: 5
            }
        }
    })
    /*******************************/
    // this is for smooth scroll on click
    /*******************************/
    $('a').on('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 90
        }, 1000);
        event.preventDefault();
        // code

    });
    </script>
</body>

</html>
