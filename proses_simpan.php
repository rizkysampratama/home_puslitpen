<?php
// Load file koneksi.php
include "koneksi.php";

// Ambil Data yang Dikirim dari Form
$id =$_POST['id'];
$nama_gubes = $_POST['nama_gubes'];
$institusi = $_POST['institusi'];
$b_ilmu = $_POST['b_ilmu'];
$tahun = $_POST['tahun'];
$naskah = $_FILES['naskah']['name'];
$tmp = $_FILES['naskah']['tmp_name'];
	
// Rename nama fotonya dengan menambahkan tanggal dan jam upload
$naskahbaru = date('dmYHis').$naskah;

// Set path folder tempat menyimpan fotonya
$path = "upload/".$naskahbaru;

// Proses upload
if(move_uploaded_file($tmp, $path)){ // Cek apakah gambar berhasil diupload atau tidak
	// Proses simpan ke Database
	$query = "INSERT INTO d_gubes VALUES('".$id."','".$nama_gubes."', '".$institusi."', '".$b_ilmu."', '".$tahun."', '".$naskahbaru."')";
	$sql = mysqli_query($connect, $query); // Eksekusi/ Jalankan query dari variabel $query

	if($sql){ // Cek jika proses simpan ke database sukses atau tidak
		// Jika Sukses, Lakukan :
		header("location: input.php"); // Redirect ke halaman index.php
	}else{
		// Jika Gagal, Lakukan :
		echo "Maaf, Terjadi kesalahan saat mencoba untuk menyimpan data ke database.";
		echo "<br><a href='form_simpan.php'>Kembali Ke Form</a>";
	}
}else{
	// Jika gambar gagal diupload, Lakukan :
	echo "Maaf, Gambar gagal untuk diupload.";
	echo "<br><a href='form_simpan.php'>Kembali Ke Form</a>";
}
?>
